/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliar;

import java.util.HashMap;
import auxiliar.Simbolo;
import java.util.Stack;
/**
 *
 * @author lucas
 */
public class TabelaSimbolos {
    private HashMap<String, Simbolo> tabelaSimbolos = new HashMap<>();
    private Stack<HashMap<String,Simbolo>> stack = new Stack<HashMap<String,Simbolo>>();
    
    public boolean isLocalEnv(){
        if (stack.isEmpty()){
            return false;
        }
        else return true;
    }
    
    public void addEnv(){
        stack.add(new HashMap<String, Simbolo>());
    }
    
    public void removeEnv(){
        stack.pop();
    }
    
    public HashMap<String, Simbolo> getHash(){
        return this.stack.lastElement();
    }
    public boolean ExistLocal(String name){
        if(stack.isEmpty()){
            return false;
        }
        return this.stack.lastElement().containsKey(name);
    }
    
    public boolean Exists(String name){
        if(tabelaSimbolos.containsKey(name)){
            return true;
        }
        else if(ExistLocal(name)){
            return true;
        }
        return false;
    }
    
    public boolean Add(Simbolo simbolo){
        if(!Exists(simbolo.getNome())){
            if(isLocalEnv()){
                stack.lastElement().put(simbolo.getNome(), simbolo);
            }
            else{
                tabelaSimbolos.put(simbolo.getNome(), simbolo);
            }
            return true;
        }
        return false;
    }
    
    public Simbolo Find(String name){
        if(tabelaSimbolos.containsKey(name)){
            return tabelaSimbolos.get(name);
        }
        else{
            return stack.lastElement().get(name);
        }
    }
    public void dump(){
        int i= 0;
        System.out.println("Variaveis Locais: ");
        while(!stack.isEmpty()){
            HashMap<String, Simbolo> hash = stack.pop();
            System.out.println("Enviornment " + i++);
            for(Simbolo simbolo: hash.values()){
                System.out.println("Tipo de dado: "+simbolo.getTipoDado());
                System.out.println("Tipo de Ident: "+simbolo.getTipoIdentificador());
                System.out.println(simbolo.nome);
                System.out.println("------------------");
            }
        }
        System.out.println("Variaveis Globais: ");

        for(Simbolo simbolo: tabelaSimbolos.values()){
            System.out.println("Tipo de dado: "+simbolo.getTipoDado());
            System.out.println("Tipo de Ident: "+simbolo.getTipoIdentificador());
            System.out.println(simbolo.nome);
            System.out.println("------------------");
        }
        
    }
    public boolean Remove(Simbolo simbolo){
         HashMap<String, Simbolo> hash = stack.lastElement();
        if(hash.containsKey(simbolo.getNome())){
            hash.remove(simbolo.getNome());
            return true;
        }
        else if(tabelaSimbolos.containsKey(simbolo.getNome())){
            tabelaSimbolos.remove(simbolo.getNome());
            return true;
        }

        return false;
    }
}
