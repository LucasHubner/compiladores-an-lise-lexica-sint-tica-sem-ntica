/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliar;

/**
 *
 * @author lucas
 */
public class Simbolo {
    String tipoIdentificador;
    String tipoDado;
    String nome;

    public Simbolo() {
    }

    public Simbolo(String tipoIdentificador, String tipoDado, String nome) {
        this.tipoIdentificador = tipoIdentificador;
        this.tipoDado = tipoDado;
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getTipoIdentificador() {
        return tipoIdentificador;
    }

    public void setTipoIdentificador(String tipoIdentificador) {
        this.tipoIdentificador = tipoIdentificador;
    }

    public String getTipoDado() {
        return tipoDado;
    }

    public void setTipoDado(String tipoDado) {
        this.tipoDado = tipoDado;
    }
}
